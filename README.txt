About this module:
This module integrates the Progistics Connectship API as a shipping method for 
Ubercart.  This module is expected to only be used by high volume shippers and 
requires a dedicated ConnectShip Windows server along with your license.  In a 
nutshell, ConnectShip is a web service that serves as an abstraction layer for 
every carrier's shipping rate calculation API. This means through one service 
call you can rate a shipment for USPS, UPS, UPS Surepost, FedEx, Canada Post 
and Canpar. 

For the D7/Commerce module check out drupal.org/project/commerce_connectship

Features (must be included in your ConnectShip license):
-P2P Delivery
-Generate Shipping labels for various printer models
-Select carriers/services
-Rate/weight markups
-Fulfillment: make packages, ship, save shipments, labels etc.

Requirements:
-Drupal 6.x + Ubercart
-A Windows server running the Progistics ConnectShip software.  

How to use this module: 

2. Enable UC ConnectShip in /admin/build/modules
3. Configure ConnectShip Settings in 
/admin/store/settings/quotes/methods/connectship.  Here you will enter your 
ConnectShip Login ID, password and public facing XML Processor URL configured 
on your ConnectShip Windows server.  You can choose your shipping department's 
printer model for label printing and enable the services that are activated
with your particular ConnectShip license.
4. Enable ConnectShip as a shipping method in admin/store/settings/quotes/methods