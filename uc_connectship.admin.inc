<?php

/**
 * ConnectShip shipping settings.
 *
 * Configures ConnectShip Settings
 */
//todo: select printer for labels @see uc_connectship_printer_models()
function uc_connectship_admin_settings() {
    $form = array();

    // Container for credentials forms.
    $form['uc_connectship_credentials'] = array(
        '#type' => 'fieldset',
        '#title' => t('ConnectShip API Credentials'),
        '#description' => t('Authorization information.'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['uc_connectship_credentials']['uc_connectship_login'] = array(
        '#type' => 'textfield',
        '#title' => t('ConnectShip Login ID'),
        '#default_value' => variable_get('uc_connectship_login', 'TEST'),
        '#required' => TRUE,
        '#description' => t('Your ConnectShip Login ID.'),
    );
    $form['uc_connectship_credentials']['uc_connectship_pass'] = array(
        '#type' => 'textfield',
        '#title' => t('ConnectShip Password'),
        '#default_value' => variable_get('uc_connectship_pass', 'TEST'),
        '#required' => TRUE,
        '#description' => t('Your ConnectShip Password.'),
    );
    $form['uc_connectship_credentials']['uc_connectship_xml_url'] = array(
        '#type' => 'textfield',
        '#title' => t('XML Processor URL'),
        '#default_value' => variable_get('uc_connectship_xml_url'),
        '#required' => TRUE,
        '#description' => t('Your ConnectShip XML Processor URL for recieving rates. Should look something like http://hostname/Progistics/XML_Processor/Server/XMLProcDLL.asp'),
    );
    $form['uc_connectship_credentials']['uc_connectship_shipper'] = array(
        '#type' => 'textfield',
        '#title' => t('Shipper Abbreviation'),
        '#default_value' => variable_get('uc_connectship_shipper'),
        '#required' => TRUE,
        '#description' => t('Your personal shipper abbreviation set in your connectship api settings.'),
    );
    $form['uc_connectship_pkg_weight'] = array(
        '#type' => 'textfield',
        '#title' => t('Max weight per package'),
        '#default_value' => variable_get('uc_connectship_pkg_weight'),
        '#required' => TRUE,
        '#description' => t('Enter the maximum weight to put in each package.  A new package will be created if this threshold is passed when requesting rates.'),
    );
    $models = uc_connectship_printer_models();

    $form['uc_connectship_printers'] = array(
        '#type' => 'fieldset',
        '#title' => t('Printer Models'),
        '#description' => t('Choose your printer model for printing labels returned from ConnectShip'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['uc_connectship_printers']['uc_connectship_printer_model'] = array(
        '#type' => 'radios',
        '#title' => check_plain($optgroup),
        '#options' => $models,
        '#default_value' => variable_get('uc_connectship_printer_model'),
    );
    //todo: checkbox for sending customer notifications after shipment
    // Container for markup forms.
    $form['uc_connectship_markups'] = array(
        '#type' => 'fieldset',
        '#title' => t('Markups'),
        '#description' => t('Modifiers to the quoted rate'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    // Form to select type of rate markup.
    $form['uc_connectship_markups']['uc_connectship_rate_markup_type'] = array(
        '#type' => 'select',
        '#title' => t('Rate Markup Type'),
        '#default_value' => variable_get('uc_connectship_rate_markup_type', 'percentage'),
        '#options' => array(
            'percentage' => t('Percentage (%)'),
            'multiplier' => t('Multiplier (×)'),
            'currency' => t('Addition (!currency)', array('!currency' => variable_get('uc_currency_sign', '$'))),
        ),
    );

    // Form to select rate markup amount.
    $form['uc_connectship_markups']['uc_connectship_rate_markup'] = array(
        '#type' => 'textfield',
        '#title' => t('ConnectShip Rate Markup'),
        '#default_value' => variable_get('uc_connectship_rate_markup', '0'),
        '#description' => t('Markup shipping rate quote by dollar amount, percentage, or multiplier.'),
    );

    $form['services'] = array(
        '#type' => 'fieldset',
        '#title' => t('Enable ConnectShip shipping services'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
        '#description' => t('NOTE: The services you select here will only work if they are active for your ConnectShip account.'),
    );
    
    // Build an options array of all available ConnectShip package services.
    $package_services = uc_connectship_package_services();
    foreach ($package_services as $optgroup => $optgroup_options) {
        $form['services'][strtr($optgroup, ' ', '-')] = array(
            '#type' => 'checkboxes',
            '#title' => check_plain($optgroup),
            '#options' => $optgroup_options,
            '#default_value' => array_keys(uc_connectship_load_services()),
        );
    }
    $form['#submit'][] = 'uc_connectship_admin_settings_submit';
    return system_settings_form($form);
}

function uc_connectship_admin_settings_submit($form, &$form_state) {


    $all_services = uc_connectship_package_services();


    foreach ($form_state['values']['services'] as $key => $value) {
        // do a test request for each to make sure they are valid with your license.
        foreach ($value as $serviceKey => $serviceVal) {
            if ($serviceVal === 0) {
                db_query("delete from uc_connectship_service where name = '%s'", $serviceKey);
            } else {
                db_query("insert into uc_connectship_service (name, title, weight) values ('%s', '%s', 0)", $serviceVal, $all_services[$key][$serviceKey]);
            }
        }
    }
}