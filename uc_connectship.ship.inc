<?php

/**
 * @file
 * UPS functions for label generation.
 */

/**
 * Shipment creation callback.
 *
 * Confirms shipment data before requesting a shipping label.
 *
 * @param $order_id
 *   The order id for the shipment.
 * @param $package_ids
 *   Array of package ids to shipped.
 *
 * @see uc_ups_fulfill_order_validate()
 * @see uc_ups_fulfill_order_submit()
 * @ingroup forms
 */
function uc_connectship_fulfill_order($form_state, $order, $package_ids) {
    $form = array();
    $pkg_types = array('Customer Supplied Package',);
    $form['order_id'] = array('#type' => 'value', '#value' => $order->order_id);
    $packages = array();
    $addresses = array();

    // Container for package data
    $form['packages'] = array(
        '#type' => 'fieldset',
        '#title' => t('Packages'),
        '#collapsible' => TRUE,
        '#tree' => TRUE,
    );
    foreach ($package_ids as $id) {
        $package = uc_shipping_package_load($id);
        if ($package) {
            foreach ($package->addresses as $address) {
                if (!in_array($address, $addresses)) {
                    $addresses[] = $address;
                }
            }
            // Create list of products and get a representative product (last one in
            // the loop) to use for some default values
            $product_list = array();
            $declared_value = 0;
            foreach ($package->products as $product) {
                $product_list[] = $product->qty . ' x ' . $product->model;
                $declared_value += $product->qty * $product->price;
            }
            // Use last product in package to determine package type
            $ups_data = db_fetch_array(db_query("SELECT pkg_type FROM {uc_ups_products} WHERE nid = %d", $product->nid));
            $product->ups = $ups_data;
            $pkg_form = array(
                '#type' => 'fieldset',
                '#title' => t('Package !id', array('!id' => $id)),
            );
            $pkg_form['products'] = array(
                '#value' => theme('item_list', $product_list)
            );
            $pkg_form['package_id'] = array(
                '#type' => 'hidden',
                '#value' => $id
            );
            $pkg_form['pkg_type'] = array(
                '#type' => 'select',
                '#title' => t('Package type'),
                '#options' => $pkg_types,
                '#default_value' => $product->ups['pkg_type'],
                '#required' => TRUE,
            );
            $pkg_form['declared_value'] = array(
                '#type' => 'textfield',
                '#title' => t('Declared value'),
                '#default_value' => $declared_value,
                '#required' => TRUE,
            );
            $pkg_form['weight'] = array(
                '#type' => 'fieldset',
                '#title' => t('Weight'),
                '#description' => t('Weight of the package. Default value is sum of product weights in the package.'),
                '#weight' => 15,
                '#theme' => 'uc_shipping_package_dimensions',
            );
            $pkg_form['weight']['weight'] = array(
                '#type' => 'textfield',
                '#title' => t('Weight'),
                '#default_value' => isset($package->weight) ? $package->weight : 0,
                '#size' => 10,
                '#maxlength' => 15,
            );
            $pkg_form['weight']['units'] = array(
                '#type' => 'select',
                '#title' => t('Units'),
                '#options' => array(
                    'lb' => t('Pounds'),
                    'kg' => t('Kilograms'),
                    'oz' => t('Ounces'),
                    'g' => t('Grams'),
                ),
                '#default_value' => isset($package->weight_units) ?
                        $package->weight_units :
                        variable_get('uc_weight_unit', 'lb'),
            );
            $pkg_form['dimensions'] = array(
                '#type' => 'fieldset',
                '#title' => t('Dimensions'),
                '#description' => t('Physical dimensions of the package.'),
                '#weight' => 20,
                '#theme' => 'uc_shipping_package_dimensions',
            );
            $pkg_form['dimensions']['length'] = array(
                '#type' => 'textfield',
                '#title' => t('Length'),
                '#default_value' => isset($product->length) ? $product->length : 1,
                '#size' => 8,
            );
            $pkg_form['dimensions']['width'] = array(
                '#type' => 'textfield',
                '#title' => t('Width'),
                '#default_value' => isset($product->width) ? $product->width : 1,
                '#size' => 8,
            );
            $pkg_form['dimensions']['height'] = array(
                '#type' => 'textfield',
                '#title' => t('Height'),
                '#default_value' => isset($product->height) ? $product->height : 1,
                '#size' => 8,
            );
            $pkg_form['dimensions']['units'] = array(
                '#type' => 'select',
                '#title' => t('Units of measurement'),
                '#options' => array(
                    'in' => t('Inches'),
                    'ft' => t('Feet'),
                    'cm' => t('Centimeters'),
                    'mm' => t('Millimeters'),
                ),
                '#default_value' => isset($product->length_units) ?
                        $product->length_units :
                        variable_get('uc_length_unit', 'in'),
            );

            $form['packages'][$id] = $pkg_form;
        }
    }

    $form = array_merge($form, uc_shipping_address_form($form_state, $addresses, $order));

    foreach (array('delivery_email', 'delivery_last_name', 'delivery_street1', 'delivery_city', 'delivery_zone', 'delivery_country', 'delivery_postal_code') as $field) {
        $form['destination'][$field]['#required'] = TRUE;
    }

    // Determine shipping option chosen by the customer
    $method = $order->quote['method'];
    $methods = module_invoke_all('shipping_method');
    if (isset($methods[$method])) {
        $services = $methods[$method]['quote']['accessorials'];
        $method_title = $services[$order->quote['accessorials']];
    }

    // Container for shipment data
    $form['shipment'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => t('Shipment data'),
        '#collapsible' => TRUE,
    );

    // Inform user of customer's shipping choice
    $form['shipment']['shipping_choice'] = array(
        '#type' => 'markup',
        '#prefix' => '<div>',
        '#value' => t('Customer selected "@method" as the shipping method and paid @rate', array('@method' => $methods[$method]['quote']['accessorials'][$order->quote['accessorials']], '@rate' => uc_currency_format($order->quote['rate']))),
        '#suffix' => '</div>',
    );

    // Pass shipping charge paid information on to validation function so it
    // can be displayed alongside actual costs
    $form['shipment']['paid'] = array(
        '#type' => 'value',
        '#value' => uc_currency_format($order->quote['rate']),
    );

    $services = uc_connectship_load_services();

    $default_service = $order->quote['accessorials'];
    $form['shipment']['service'] = array(
        '#type' => 'select',
        '#title' => t('ConnectShip service'),
        '#options' => $services,
        '#default_value' => $order->quote['accessorials'],
    );
    $today = getdate();
    $form['shipment']['ship_date'] = array(
        '#type' => 'date',
        '#title' => t('Ship date'),
        '#default_value' => array(
            'year' => $today['year'],
            'month' => $today['mon'],
            'day' => $today['mday']
        ),
    );
    $form['shipment']['expected_delivery'] = array(
        '#type' => 'date',
        '#title' => t('Expected delivery'),
        '#default_value' => array(
            'year' => $today['year'],
            'month' => $today['mon'],
            'day' => $today['mday']
        ),
    );
    $form['submit'] = array('#type' => 'submit', '#value' => t('Review shipment'));
    return $form;
}

/**
 * Passes final information into shipment object.
 *
 * @see uc_connectship_fulfill_order()
 * @see uc_ups_confirm_shipment()
 */
function uc_connectship_fulfill_order_validate($form, &$form_state) {
    $errors = form_get_errors();

    if (isset($errors)) {
        // Some required elements are missing - don't bother with making
        // a UPS API call until that gets fixed.
        return;
    }

    $origin = array();
    $destination = array();
    $packages = array();
    foreach ($form_state['values'] as $key => $value) {
        if (substr($key, 0, 7) == 'pickup_') {
            $origin[$key] = $value;
        }
        if (substr($key, 0, 9) == 'delivery_') {
            $destination[$key] = $value;
        }
    }
    $_SESSION['connectship'] = array();
    $_SESSION['connectship']['origin'] = $origin;
    //todo: need function to check if destination is residential
    //$destination->residential = variable_get('uc_ups_residential_quotes', FALSE);

    $_SESSION['connectship']['destination'] = $destination;
    $packages = array();
    foreach ($form_state['values']['packages'] as $id => $pkg_form) {
        $dimensions_string = $pkg_form['dimensions']['length'] . 'X' . $pkg_form['dimensions']['width'] . 'X' . $pkg_form['dimensions']['height'];
        $packages[] = array(
            'PKG' => array(
                'PKGWEIGHT' => array(
                    'WEIGHTVALUE' => $pkg_form['weight']['weight'],
                    'WEIGHTUNITS' => $pkg_form['weight']['units'],
                ),
                'DIMENSION' => array(
                    'DIMUNITS' => 'in',
                    'DIMVALUE' => $dimensions_string,
                ),
                'PACKAGING' => 'CUSTOM',
                'REFERENCE' =>
                array(
                    'CONSIGNEEREFERENCE' => 'Order #' . $form_state['values']['order_id'],
                ),
                'PACKAGESERVICEOPTIONS' => array(
                    'DECLAREDVALUE' => array(
                        'CURRENCYCODE' => 'USD',
                        'MONETARYVALUE' => 440,
                    ),
                    'PROOF' => array(
                        'PROOFFLAG' => TRUE,
                        'PROOFSIGNATURE' => TRUE, //note: can't request both adult and regular sig required simultaniously!
                    //'PROOFADULTSIGNATURE' => TRUE, //todo:figure out adult sig
                    ),
                ),
            ),
        );
        $package = uc_shipping_package_load($id);
        $package->pkg_type = $pkg_form['pkg_type'];
        $package->value = $pkg_form['declared_value'];
        $package->length = $pkg_form['dimensions']['length'];
        $package->width = $pkg_form['dimensions']['width'];
        $package->height = $pkg_form['dimensions']['height'];
        $package->length_units = $pkg_form['dimensions']['units'];
        $package->qty = 1;
        $_SESSION['connectship']['packages'][$id] = $package;
    }
    $_SESSION['connectship']['connectship_packages'] = $packages;
    $_SESSION['connectship']['service'] = $form_state['values']['shipment']['service'];
    $_SESSION['connectship']['paid'] = $form_state['values']['shipment']['paid'];
    $_SESSION['connectship']['ship_date'] = $form_state['values']['shipment']['ship_date'];
    $_SESSION['connectship']['expected_delivery'] = $form_state['values']['shipment']['expected_delivery'];
    $_SESSION['connectship']['order_id'] = $form_state['values']['order_id'];
    //make sure this is a valid address
    //$valid_addr = uc_connectship_validate_address($_SESSION['connectship']['destination']);
    $shipment_request = uc_connectship_build_shipment_request($packages, $_SESSION['connectship']['origin'], $_SESSION['connectship']['destination'], $_SESSION['connectship']['service'], FALSE);

    $response = uc_connectship_api_request($shipment_request->asXml());

    $response_status = (string) $response->RESPONSE->RESPONSESTATUSDESCRIPTION;
    $_SESSION['connectship']['rate']['type'] = t('Total Charges');
    $_SESSION['connectship']['rate']['currency'] = (string) $response->RATEDSHIPMENT->TOTALCHARGES->CURRENCYCODE;
    $_SESSION['connectship']['rate']['amount'] = (string) $response->RATEDSHIPMENT->TOTALCHARGES->MONETARYVALUE;

    if ($response_status != 'Success') {
        $error_msg = check_plain($response->RESPONSE->RESPONSEERROR->ERRORDESCRIPTION);
        form_set_error('shipment][service', $error_msg);        
        //todo: watchdog
    }
}

/**
 * Passes final information into shipment object.
 *
 * @see uc_ups_fulfill_order()
 * @see uc_ups_confirm_shipment()
 */
function uc_connectship_fulfill_order_submit($form, &$form_state) {
    $form_state['redirect'] = 'admin/store/orders/' . $form_state['values']['order_id'] . '/shipments/connectship';
}

/**
 * Last chance for user to review shipment.
 *
 * @see uc_connectship_confirm_shipment_submit()
 * @see theme_uc_connectship_confirm_shipment()
 * @ingroup forms
 */
function uc_connectship_confirm_shipment($order) {
    $form = array();
    $form['digest'] = array('#type' => 'hidden', '#value' => $_SESSION['connectship']);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Request Pickup'));
    return $form;
}

/**
 * Displays final shipment information for review.
 *
 * @see uc_connectship_confirm_shipment()
 * @ingroup themeable
 */
function theme_uc_connectship_confirm_shipment($form) {
    $output = '';
    $output .= '<div class="shipping-address"><b>' . t('Ship from:') . '</b><br />';
    $output .= uc_address_format(
            check_plain($_SESSION['connectship']['origin']['pickup_first_name']), check_plain($_SESSION['connectship']['origin']['pickup_last_name']), check_plain($_SESSION['connectship']['origin']['pickup_company']), check_plain($_SESSION['connectship']['origin']['pickup_street1']), check_plain($_SESSION['connectship']['origin']['pickup_street2']), check_plain($_SESSION['connectship']['origin']['pickup_city']), check_plain($_SESSION['connectship']['origin']['pickup_zone']), check_plain($_SESSION['connectship']['origin']['pickup_postal_code']), check_plain($_SESSION['connectship']['origin']['pickup_country'])
    );
    $output .= '<br />' . check_plain($_SESSION['connectship']['origin']['pickup_email']);
    $output .= '</div>';
    $output .= '<div class="shipping-address"><b>' . t('Ship to:') . '</b><br />';
    $output .= uc_address_format(
            check_plain($_SESSION['connectship']['destination']['delivery_first_name']), check_plain($_SESSION['connectship']['destination']['delivery_last_name']), check_plain($_SESSION['connectship']['destination']['delivery_company']), check_plain($_SESSION['connectship']['destination']['delivery_street1']), check_plain($_SESSION['connectship']['destination']['delivery_street2']), check_plain($_SESSION['connectship']['destination']['delivery_city']), check_plain($_SESSION['connectship']['destination']['delivery_zone']), check_plain($_SESSION['connectship']['destination']['delivery_postal_code']), check_plain($_SESSION['connectship']['destination']['delivery_country'])
    );
    $output .= '<br />' . check_plain($_SESSION['connectship']['destination']['delivery_email']);
    $output .= '</div>';
    $output .= '<div class="shipment-data">';
    $accessorials = uc_connectship_accessorials();
    $method = $accessorials[$_SESSION['connectship']['service']];
    $output .= '<b>' . $method . '</b><br />';
    $context = array(
        'revision' => 'themed',
        'type' => 'amount',
    );
    $output .= '<i>' . check_plain($_SESSION['connectship']['rate']['type']) . '</i>: ' . uc_price($_SESSION['connectship']['rate']['amount'], $context) . ' (' . check_plain($_SESSION['connectship']['rate']['currency']) . ') -- ';
    $output .= '<i>' . t('Paid') . '</i>: ' . $_SESSION['connectship']['paid'] . '<br />';
    $ship_date = $_SESSION['connectship']['ship_date'];
    $output .= 'Ship date: ' . format_date(gmmktime(12, 0, 0, $ship_date['month'], $ship_date['day'], $ship_date['year']), 'custom', variable_get('uc_date_format_default', 'm/d/Y'));
    $exp_delivery = $_SESSION['connectship']['expected_delivery'];
    $output .= '<br />Expected delivery: ' . format_date(gmmktime(12, 0, 0, $exp_delivery['month'], $exp_delivery['day'], $exp_delivery['year']), 'custom', variable_get('uc_date_format_default', 'm/d/Y'));
    $output .= "</div>\n<br style=\"clear: both;\" />";
    $output .= drupal_render($form);
    return $output;
}

/**
 * Generates label and schedules pickup of the shipment.
 *
 * @see uc_connectship_confirm_shipment()
 */
function uc_connectship_confirm_shipment_submit($form, &$form_state) {
    // Request pickup using parameters in form.
    $order_id = $_SESSION['connectship']['order_id'];
    $request = uc_connectship_build_shipment_request($_SESSION['connectship']['connectship_packages'], $_SESSION['connectship']['origin'], $_SESSION['connectship']['destination'], $_SESSION['connectship']['service'], TRUE);
    $response = uc_connectship_api_request($request->asXml());
    $response_status = (string) $response->RESPONSE->RESPONSESTATUSDESCRIPTION;
    if ($response_status != 'Success') {
        $error_msg = check_plain($response->RESPONSE->RESPONSEERROR->ERRORDESCRIPTION);
        drupal_set_message(t('@error_msg', array('@error_msg' => $error_msg,)), 'error');
    }
    $shipment = new stdClass();
    $shipment->order_id = $order_id;
    $origin = $_SESSION['connectship']['origin'];
    foreach(array_keys(&$origin) as $pickupKey){
        $newPickupKey = substr($pickupKey, 7);
        $origin[$newPickupKey] = $origin[$pickupKey];
        unset($origin[$pickupKey]);
    }
    $dest = $_SESSION['connectship']['destination'];
    foreach(array_keys(&$dest) as $destKey){
        $newDestKey = substr($destKey, 9);
        $dest[$newDestKey] = $dest[$destKey];
        unset($dest[$destKey]);
    }
    $shipment->origin = $origin;
    $shipment->destination = $dest;
    $shipment->packages = $_SESSION['connectship']['packages'];
    $shipment->shipping_method = 'connectship';
    $shipment->accessorials = $_SESSION['connectship']['service'];
    if (strstr($_SESSION['connectship']['service'], 'UPS')) {
        $shipment->carrier = t('UPS');
    } elseif (strstr($_SESSION['connectship']['service'], 'USPS')) {
        $shipment->carrier = t('USPS');
    } else {
        $shipment->carrier = t('Unknown');
    }

    $shipment->cost = $_SESSION['connectship']['rate']['amount'];
    $shipment->tracking_number = check_plain($response->RATEDSHIPMENT->RATEDPACKAGE->DELIVERYINFO->TRACKINGNUMBER);
    $ship_date = $_SESSION['connectship']['ship_date'];
    $shipment->ship_date = gmmktime(12, 0, 0, $ship_date['month'], $ship_date['day'], $ship_date['year']);
    $exp_delivery = $_SESSION['connectship']['expected_delivery'];
    $shipment->expected_delivery = gmmktime(12, 0, 0, $exp_delivery['month'], $exp_delivery['day'], $exp_delivery['year']);

    foreach ($response->RATEDSHIPMENT->RATEDPACKAGE as $package_results) {
        $package = & current($shipment->packages);
        $package->tracking_number = (string) $package_results->DELIVERYINFO->TRACKINGNUMBER;
        $connectship_package_data = array(
            'package_id' => $package->package_id,
            'tracking_number' => $package->tracking_number,
            'msn' => (string) $package_results->LABELREQUESTINFO->MSN,
            'bundle_id' => (string) $package_results->LABELREQUESTINFO->BUNDLEID,
            'service_carrier' => (string) $package_results->LABELREQUESTINFO->SC,
        );
        uc_connectship_save_package_data($connectship_package_data);
        $label_string = (string) $package_results->LABELREQUESTINFO->PDFCONTENT;
        if (file_check_directory(file_create_path('connectship_labels'), FILE_CREATE_DIRECTORY)) {
            $label_path = file_create_path('./sites/default/files/connectship_labels') . '/label' . $package->tracking_number . '.pdf';
            if ($label_file = fopen($label_path, 'wb')) {
                fwrite($label_file, base64_decode($label_string));
                fclose($label_file);
                $package->label_image = 'connectship_labels/' . basename($label_path);
            } else {
                drupal_set_message(t('Could not open a file to save the label image.'), 'error');
            }
        } else {
            drupal_set_message(t('Could not find or create the directory "connectship_labels" in the file system path.'), 'error');
        }
        unset($package);
        next($shipment->packages);
    }

    uc_shipping_shipment_save($shipment);

    unset($_SESSION['connectship']);

    $form_state['redirect'] = 'admin/store/orders/' . $order_id . '/shipments';
}

/*
 * Save returned package data in table uc_connectship_package_data, for later use i.e. voids
 */

function uc_connectship_save_package_data($package_data){
    $package_id = $package_data['package_id'];
    $package_exists = db_result(db_query("SELECT package_id from uc_connectship_package_data where package_id = $package_id"));
    if(!$package_exists){
            $success = db_query("INSERT INTO {uc_connectship_package_data} (package_id, tracking_number, msn, bundle_id, service_carrier) values (%d, '%s', %d, %d, '%s')", $package_data['package_id'], $package_data['tracking_number'], $package_data['msn'], $package_data['bundle_id'], $package_data['service_carrier'] );
    }else{
        //update this package with new shipment info
        $success = db_query("UPDATE uc_connectship_package_data set tracking_number = '%s', service_carrier = '%s', msn = %d, bundle_id = %d", $package_data['tracking_number'], $package_data['service_carrier'], $package_data['msn'], $package_data['bundle_id']);
    }

    if($success){
        drupal_set_message('Package saved.');
    }else{
        drupal_set_message('There were errors saving the package!  This is probably due to voiding a shipment outside our system.', 'error');
    }
}